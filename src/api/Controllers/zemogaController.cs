﻿using System;
using zemoga.core.interfaces.contratos.servicios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace zemoga.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class zemogaController : ControllerBase
    {
        private readonly IzemogaServicio _zemogaServicio;
        private readonly ILogger<zemogaController> _logger;

        public zemogaController(ILogger<zemogaController> logger,
                                          IzemogaServicio zemogaServicio)
        {
            _logger = logger;
            _zemogaServicio = zemogaServicio;
        }

        [HttpGet]
        public IActionResult Get() => Ok(_zemogaServicio.ObtenerTodos());

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(Guid id) => Ok(_zemogaServicio.ObtenerPorId(id));

    }
}
