﻿using zemoga.core.extensiones;
using Microsoft.Extensions.DependencyInjection;

namespace zemoga.core
{
    public static class CoreExtension
    {
        public static IServiceCollection AddCore(this IServiceCollection servicios)
        {
            //Enable AutoMapper
            servicios.AddAutoMapper(typeof(zemogaMappingProfile));

            return servicios;
        }

    }
}
