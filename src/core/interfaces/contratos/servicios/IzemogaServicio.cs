using System;
using System.Collections.Generic;
using zemoga.core.dto;

namespace zemoga.core.interfaces.contratos.servicios
{
    public interface IzemogaServicio
    {
        zemogaDto ObtenerPorId(Guid id);
        List<zemogaDto> ObtenerTodos();
    }
}