
using AutoMapper;
using zemoga.core.dto;
using zemoga.core.entities;
using zemoga.core.extensiones;
using zemoga.core.interfaces.contratos;
using zemoga.core.interfaces.contratos.servicios;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace zemoga.services.test
{
    public class zemogaServicioTest
    {
        private readonly IMapper _mapper;
        private readonly Mock<IRepositorioWrapper> _mockRepositorioWrapper;

        public zemogaServicioTest()
        {
            _mapper = new MapperConfiguration(opts => { opts.AddProfile(typeof(zemogaMappingProfile)); }).CreateMapper();
            _mockRepositorioWrapper = new Mock<IRepositorioWrapper>();
        }

        [Fact]
        public void Should_ObtenerPorId()
        {
            // Arrange
            var zemogaServicio = ObtenerzemogaServicio();
            _mockRepositorioWrapper
                .Setup(mrw => mrw.zemogaRepositorio.BuscarPorCondicion(It.IsAny<Expression<Func<zemogaEntity, bool>>>()))
                .Returns(ObtenerEntidadesQueryable());

            // Act
            var resultado = zemogaServicio.ObtenerPorId(It.IsAny<Guid>());

            // Assert
            Assert.IsType<zemogaDto>(resultado);
            _mockRepositorioWrapper
                .Verify(mrw => mrw.zemogaRepositorio.BuscarPorCondicion(It.IsAny<Expression<Func<zemogaEntity, bool>>>()), Times.Once);
        }

        [Fact]
        public void Should_ObtenerTodos()
        {
            // Arrange
            var zemogaServicio = ObtenerzemogaServicio();
            _mockRepositorioWrapper
                .Setup(mrw => mrw.zemogaRepositorio.ObtenerTodo())
                .Returns(ObtenerEntidadesQueryable());
            // Act
            var resultado = zemogaServicio.ObtenerTodos();

            // Assert
            Assert.IsType<List<zemogaDto>>(resultado);
            Assert.Single(resultado);
            _mockRepositorioWrapper
                .Verify(mrw => mrw.zemogaRepositorio.ObtenerTodo(), Times.Once);
        }

        [Fact]
        public void Should_NewValidationObtenerTodos()
        {
            // Arrange
            var theValidationIsTrue = 1 > 0;

            // Assert
            Assert.True(theValidationIsTrue);
        }

        #region Private Methods

        private IzemogaServicio ObtenerzemogaServicio()
        {
            return new zemogaServicio(_mapper, _mockRepositorioWrapper.Object);
        }

        private IQueryable<zemogaEntity> ObtenerEntidadesQueryable()
        {
            var listado = new List<zemogaEntity>
            {
                new zemogaEntity()
            };

            return listado.AsQueryable();
        }
        #endregion
    }
}
