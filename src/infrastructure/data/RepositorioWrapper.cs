using zemoga.core.interfaces.contratos;

namespace zemoga.infrastructure.data
{
    public class RepositorioWrapper : IRepositorioWrapper
    {
        private readonly zemogaContexto _zemogaContexto;
        private IzemogaRepositorio _zemogaRepositorio;

        public RepositorioWrapper(zemogaContexto zemogaContexto)
        {
            _zemogaContexto = zemogaContexto;
        }

        public IzemogaRepositorio zemogaRepositorio
        {
            get
            {
                if (_zemogaRepositorio == null)
                {
                    _zemogaRepositorio = new zemogaRepositorio(_zemogaContexto);
                }
                return _zemogaRepositorio;
            }
        }

        public void ConfirmarTransaccion()
        {
            _zemogaContexto.ConfirmarTransaccion();
        }

        public void Guardar()
        {
            _zemogaContexto.SaveChanges();
        }

        public void IniciarTransaccion()
        {
            _zemogaContexto.IniciarTransaccion();
        }

        public void RevertirTransaccion()
        {
            _zemogaContexto.RevertirTransaccion();
        }
    }
}