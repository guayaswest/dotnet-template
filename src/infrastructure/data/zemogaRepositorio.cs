using zemoga.core.entities;
using zemoga.core.interfaces.contratos;

namespace zemoga.infrastructure.data
{
    public class zemogaRepositorio : RepositorioGenerico<zemogaEntity>, IzemogaRepositorio
    {
        public zemogaRepositorio(zemogaContexto zemogaContexto) : base(zemogaContexto)
        {

        }
    }
}