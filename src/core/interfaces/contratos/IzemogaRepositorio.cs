using zemoga.core.entities;

namespace zemoga.core.interfaces.contratos
{
    /// <summary>
    /// Repositorio Ejemplo
    /// </summary>
    public interface IzemogaRepositorio : IRepositorioGenerico<zemogaEntity>
    {

    }
}