using zemoga.core.entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace zemoga.infrastructure.data
{
    public class zemogaContexto : DbContext
    {
        private IDbContextTransaction contextoTransaction;

        public zemogaContexto(DbContextOptions<zemogaContexto> options) : base(options) { }

        public DbSet<zemogaEntity> zemogaEntity { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            CargarDatosIniciales(modelBuilder);
        }

        public virtual void IniciarTransaccion()
        {
            contextoTransaction = Database.BeginTransaction();
        }

        public virtual void ConfirmarTransaccion()
        {
            try
            {
                SaveChanges();
                contextoTransaction.Commit();
            }
            finally
            {
                contextoTransaction.Dispose();
            }
        }

        public virtual void RevertirTransaccion()
        {
            if (contextoTransaction != null)
            {
                contextoTransaction.Rollback();
                contextoTransaction.Dispose();
            }
        }

        private static void CargarDatosIniciales(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new zemogaSemilla());
        }
    }
}