﻿using AutoMapper;
using zemoga.core.dto;
using zemoga.core.entities;

namespace zemoga.core.extensiones
{
    public class zemogaMappingProfile : Profile
    {
        public zemogaMappingProfile()
        {
            CreateMap<zemogaEntity, zemogaDto>();
        }
    }
}
