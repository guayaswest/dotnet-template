﻿using zemoga.core.interfaces.contratos.servicios;
using Microsoft.Extensions.DependencyInjection;

namespace zemoga.services
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddServicios(this IServiceCollection servicios)
        {
            servicios.AddScoped<IzemogaServicio, zemogaServicio>();

            return servicios;
        }
    }
}
