using System;
using System.Linq;
using System.Linq.Expressions;
using zemoga.core.interfaces.contratos;
using Microsoft.EntityFrameworkCore;

namespace zemoga.infrastructure.data
{
    public class RepositorioGenerico<TEntidad> : IRepositorioGenerico<TEntidad> where TEntidad : class
    {
        protected readonly zemogaContexto _zemogaContexto;

        public RepositorioGenerico(zemogaContexto zemogaContexto)
        {
            _zemogaContexto = zemogaContexto;
        }

        public void Actualizar(TEntidad entidad)
        {
            _zemogaContexto.Set<TEntidad>().Update(entidad);
            _zemogaContexto.SaveChanges();
        }

        public IQueryable<TEntidad> BuscarPorCondicion(Expression<Func<TEntidad, bool>> expresion)
        {
            return _zemogaContexto.Set<TEntidad>().Where(expresion).AsNoTracking();
        }

        public void Eliminar(TEntidad entidad)
        {
            _zemogaContexto.Set<TEntidad>().Remove(entidad);
        }

        public void Insertar(TEntidad entidad)
        {
            _zemogaContexto.Set<TEntidad>().Add(entidad);
            _zemogaContexto.SaveChanges();
        }

        public IQueryable<TEntidad> ObtenerTodo()
        {
            return _zemogaContexto.Set<TEntidad>().AsNoTracking();
        }
    }
}