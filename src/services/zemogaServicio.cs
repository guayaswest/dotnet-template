using zemoga.core.dto;
using zemoga.core.interfaces.contratos;
using zemoga.core.interfaces.contratos.servicios;
using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;

namespace zemoga.services
{
    public class zemogaServicio : IzemogaServicio
    {
        private readonly IMapper _mapper;
        private readonly IRepositorioWrapper _repositorioWrapper;

        public zemogaServicio(IMapper mapper,
                                        IRepositorioWrapper repositorioWrapper)
        {
            _mapper = mapper;
            _repositorioWrapper = repositorioWrapper;
        }

        public zemogaDto ObtenerPorId(Guid id)
        {
            var entity = _repositorioWrapper
                .zemogaRepositorio
                .BuscarPorCondicion(smp => smp.Id == id)
                .FirstOrDefault();
            return _mapper.Map<zemogaDto>(entity);
        }

        public List<zemogaDto> ObtenerTodos()
        {
            var entities = _repositorioWrapper.zemogaRepositorio.ObtenerTodo();
            return _mapper.Map<List<zemogaDto>>(entities);
        }

    }
}